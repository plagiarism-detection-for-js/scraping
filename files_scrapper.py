import scrapy
from scrapy.crawler import CrawlerProcess
import urllib.request

with open("links_without_duplicates.txt", "r") as f:
    links = [url.strip() for url in f.readlines()]

class FilesSpider(scrapy.Spider):
    name = "FilesSpider"
    def start_requests(self):
        for count, url in enumerate(links):
            yield scrapy.Request(url, self.parse)

    def parse(self, response):
        Links = response.css('div[class="f4 text-normal"] a::attr(href)').extract()
        for i in range(len(Links)):
            link = "https://raw.githubusercontent.com" + Links[i].replace('blob/', '')
            name = "Codes/" + Links[i].replace('/', '_')
            urllib.request.urlretrieve(link, name)

if __name__ == "__main__":
    process = CrawlerProcess({
        'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
    })

    process.crawl(FilesSpider)
    process.start()
