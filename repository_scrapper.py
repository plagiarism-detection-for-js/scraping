import pandas as pd

def create_links():
    dates_list = pd.date_range(start='08/11/2018', end='30/12/2018')
    beginning = "https://github.com/search?p="
    end = "+language%3AJavaScript&type=Repositories"
    links = []
    for date in dates_list:
        for i in range (100):
            links.append(beginning+str(i+1)+"&q=created%3A"+str(date.strftime('%Y-%m-%d'))+end)

    return links

links = create_links()

import scrapy
from scrapy.crawler import CrawlerProcess
from time import sleep

class RepoSpider(scrapy.Spider):
    name = "RepoSpider"
    def start_requests(self):
        for count, url in enumerate(links):
            yield scrapy.Request(url, self.parse)
            if (count < 30):
                sleep(20)
            else:
                sleep(9)

    def parse(self, response):
        Links = response.css('a[class="v-align-middle"]').xpath('@href').extract()

        #save results to file
        with open("links.txt", "a") as f:
            for link in Links:
                f.write("https://github.com" + link+"/search?l=javascript")
                f.write("\n")
            f.close()

if __name__ == "__main__":
    process = CrawlerProcess({
        'USER_AGENT': 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1)'
    })

    process.crawl(RepoSpider)
    process.start()
