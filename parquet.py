import os
import pandas as pd
import pyarrow as pa
import pyarrow.parquet as pq

my_list = []
for filename in os.listdir('Clear_codes'):
    with open(f"Clear_codes/{filename}", "r", encoding="ISO-8859-1") as file:
        my_file = file.read()
        my_list.append(my_file)

df = pd.DataFrame(my_list)

table = pa.Table.from_pandas(df)
pq.write_table(table, 'D:/data1.paraquet')

