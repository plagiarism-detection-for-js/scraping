import re
import os

for filename in os.listdir('Codes'):
    with open(f"Codes/{filename}", "r", encoding="ISO-8859-1") as file:
            my_file = file.read()

    pattern = r"(\".*?\"|\'.*?\')|(/\*.*?\*/|//[^\r\n]*$)"
    regex = re.compile(pattern, re.MULTILINE | re.DOTALL)

    def replacer(match):
        if match.group(2) is not None:
            return ""
        else:
            return match.group(1)

    new_file = regex.sub(replacer, my_file)

    f = open(f'Clear_codes/{filename}', "w+", encoding='utf-8')
    f.write(new_file)


